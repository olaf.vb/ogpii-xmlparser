package myMiniGUIGenarator;

import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.io.File;
import java.lang.reflect.*;
import javax.swing.*;

import org.omg.CORBA.PRIVATE_MEMBER;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.apache.xerces.parsers.DOMParser;
//import org.apache.*;

public class DOMUIParser {
	private Element root;
	private JPanel app;
	private  JFrame f;
	private File $document;

//TODO add constructor and main
//e.g. $dparser = new org.apache.xerces.parsers.DOMParser();
// 	   $document = new File(fileName);
	public DOMUIParser(Document doc)
	{
		buildUI(doc);
	}


	protected void buildUI(Document doc){
		root = doc.getDocumentElement();
		app = new JPanel();
		processNode(root, app);
		f = new JFrame();
		f.setTitle(getTitle());
		f.getContentPane().add(app);
		f.pack();
		f.setVisible(true);
	}

	private String getTitle() {
		NodeList children = root.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			if(children.item(i).getNodeName().equals("title"))
				return children.item(i).getNodeValue();
		}
		return null;
	}

	private String getLabel(Node wchild) {
		NodeList children = wchild.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			if(children.item(i).getNodeName().equals("text"))
				return children.item(i).getNodeValue();
		}
		return null;
	}

	private void processNode(Node n, JComponent container){
		if(n.getNodeType()==Node.ELEMENT_NODE)
			if(processWidgetElement((Element)n, container)) return;
		NodeList nl = n.getChildNodes();
		if(nl.getLength()>0)
			for(int i=0; i<nl.getLength(); i++)
				processNode(nl.item(i), container);
	}

	/* Processes a widget element, adds it to the container 
	 * @param e				an element describing a widget 
	 * @param container		the container to which the widget should be added
	 * @return 				true if the descendants of e are processed by this method
	 * 						false otherwise
	 * @pre	e != null
	 * @pre e.getChildNodes() != null
	 * @pre e.getChildNodes().getLength() > 1
	 * @pre container != null
	 * */
	private boolean processWidgetElement(Element element, JComponent container) {
		Class cl;
		Constructor ct;
		try {
			cl = Class.forName(element.getNodeName());
			ct = cl.getConstructor();

			JComponent comp = (JComponent) cl.newInstance();
			comp.setSize(100, 100);

			NodeList children = element.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {

				if (children.item(i).getNodeName().equals("text"))
				{
					Method method = cl.getMethod("setText", String.class);
					method.invoke(comp, children.item(i).getTextContent());
				}
				else
				{
					applyMethod(comp, cl, children.item(i).getNodeName(), children.item(i).getTextContent());
				}
			}

			System.out.println("");
			container.add(comp);

			//TODO 


		} catch (ClassNotFoundException e)
		{
			System.out.println("Error: class '" + element.getNodeName() + "' not found.");
			element = null;
			container = null;
		}
		catch (Exception e)
		{

		}

		if (element == null || container == null) 
			return false;
		else if (element.getChildNodes() == null) 
			return false;
		else if (element.getChildNodes().getLength() < 1)
			return false;
		else {
			return true;
		}
	}
	
	private GridLayout MakeGroup(Element element){
		GridLayout gridLayout = new GridLayout();
		String rows = element.getAttribute("rows");
		String columns = element.getAttribute("columns");
		return gridLayout;
}


	private Object applyConstructor(Class cl, String input) throws Exception
	{
		int args = countChar(input, ',') + 1;
		
		if(cl.equals(javax.swing.Icon.class))
			cl = javax.swing.ImageIcon.class;
		Constructor[] constructors = cl.getConstructors();

		for(int i = 0; i < constructors.length; ++i)
		{
			try
			{
				if(constructors[i].getParameterCount() == args)
				{
					return tryParseParameters(constructors[i], input);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}


		System.out.println("Unknown constructor: " + cl.getName());
		throw new Exception();
	}



	private void applyMethod(Object comp, Class cl, String methodName, String input)
	{
		int args = countChar(input, ',') + 1;

		Method[] methods = cl.getMethods();

		for(int i = 0; i < methods.length; ++i)
		{
			try
			{
				if(methods[i].getParameterCount() == args && methods[i].getName().equals(methodName))
				{
					if(tryParseParameters(comp, methods[i], input))
						return;
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		System.out.println("Unknown method " + methodName);
	}



	private boolean tryParseParameters(Object comp, Method method, String input) throws Exception
	{
		int argsCount = countChar(input, ',') + 1;
		String[] args = input.split(",");
		Class[] clargs = method.getParameterTypes();
		Object[] params = new Object[argsCount];


		for(int i = 0; i < args.length; ++i)
		{
			if(clargs[i] == int.class)
			{
				params[i] = Integer.parseInt(args[i]);
			}
			else if (clargs[i] == boolean.class)
			{
				params[i] = Boolean.parseBoolean(args[i]);
			}
			else if(clargs[i] == String.class)
			{
				params[i] = args[i];
			}
			else
			{
				params[i]  = applyConstructor(clargs[i],args[i]);
			}
		}

		method.invoke(comp, params);
		return true;

	}




	private Object tryParseParameters(Constructor method, String input) throws Exception
	{
		int argsCount = countChar(input, ',') + 1;
		String[] args = input.split(",");
		Class[] clargs = method.getParameterTypes();
		Object[] params = new Object[argsCount];


		for(int i = 0; i < args.length; ++i)
		{
			if(clargs[i] == int.class)
			{
				params[i] = Integer.parseInt(args[i]);
			}
			else if (clargs[i] == boolean.class)
			{
				params[i] = Boolean.parseBoolean(args[i]);
			}
			else if(clargs[i] == String.class)
			{
				params[i] = args[i];
			}
			else
			{
				params[i]  = applyConstructor(clargs[i],args[i]);
			}
		}

		return method.newInstance(params);

	}


	private int countChar(String str, Character ch)
	{
		int chars = 0;
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == ch) ++chars;
		}
		return chars;
	}






}